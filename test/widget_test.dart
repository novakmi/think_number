// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';

import 'package:think_number/main.dart';

void main() {
  testWidgets('Think a number smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(ThinkNumberApp());

    // Verify that our counter starts at 0.
    expect(find.text('OK'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // TODO add more
  });
}
