import 'dart:async';
import 'dart:ui';

import 'package:package_info/package_info.dart';

//based on https://github.com/flutter/plugins/blob/master/packages/package_info/example/lib/main.dart
PackageInfo _packageInfo = PackageInfo(
  appName: 'Unknown',
  packageName: 'Unknown',
  version: 'Unknown',
  buildNumber: 'Unknown',
);

Future<void> initPackageInfo() async {
  final PackageInfo info = await PackageInfo.fromPlatform();
  _packageInfo = info;
}

String getVersion() {
  return _packageInfo.version;
}

Color getBackground() {
  return Color(0xFFECECCE);
}
