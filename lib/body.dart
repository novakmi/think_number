import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:think_number/bloc.dart';

class ThinkNumberBody extends StatefulWidget {
  @override
  _ThinkNumberBodyState createState() => _ThinkNumberBodyState();
}

class _ThinkNumberBodyState extends State<ThinkNumberBody>
    with SingleTickerProviderStateMixin {
  //Animation based on this tutorial
  //https://code.tutsplus.com/tutorials/google-flutter-from-scratch-animating-widgets--cms-31709
  AnimationController? _controller = null;
  AnimationController get controller => _controller!;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1500));
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(child: BlocBuilder<ThinkBloc, ThinkState>(
        builder: (BuildContext context, ThinkState state) {
      bool isResult = state.text == "result";
      _controller!.reset();
      if (isResult) _controller!.repeat(reverse: true);
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                child: Text(
                  ThinkState.getStateLocalizedText(context, state),
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 45),
                )),
            Expanded(
                child: Visibility(
                    child: SingleChildScrollView(
                      child: Column(children: <Widget>[
                        Text(
                          '${state.result}',
                          style: TextStyle(
                              fontWeight: FontWeight.w900,
                              fontSize: 85,
                              color: Colors.green),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: ScaleTransition(
                            scale: controller,
                            child: Icon(
                              Icons.lightbulb_outline,
                              size: 110,
                              color: Colors.lightGreen,
                            ),
                          ),
                        ),
                      ]),
                    ),
                    visible: isResult)),
          ]);
    }));
  }
}
