import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:think_number/bloc.dart';
import 'package:think_number/common.dart';
import 'package:think_number/l10n/localization.dart';
import 'package:think_number/storage.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  RangeValues _rangeValues = RangeValues(globalStorage.lower.toDouble(),
      globalStorage.upper.toDouble());
  RangeLabels _labels = RangeLabels(globalStorage.lower.toDouble().toString(),
  globalStorage.upper.toDouble().toString());
  static final MaterialColor sliderColor = Colors.indigo;
  static final MaterialColor sliderInactiveColor = Colors.blue;

  @override
  void initState() {
    super.initState();
    _rangeValues =  RangeValues(globalStorage.lower.toDouble(),
        globalStorage.upper.toDouble());
    print('_SettingsState initState');
  }

  @override
  Widget build(BuildContext context) {
    Widget buildSlider() {
      return SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTickMarkColor: sliderColor,
          inactiveTrackColor: sliderInactiveColor,
          activeTrackColor: sliderColor,
          thumbColor: sliderColor,
          valueIndicatorColor: sliderColor,
          valueIndicatorTextStyle: TextStyle(
              color: Colors.white, height: 1.0, fontWeight: FontWeight.bold),
        ),
        child: RangeSlider(
          min: 1.0,
          max: 20.0,
          values:  _rangeValues,
          divisions: 19,
          labels: _labels,
          onChanged: (RangeValues newValues) {
            setState(() {
              if (newValues.end - newValues.start >= 4.0) {
                 _rangeValues = newValues;
                 _labels = RangeLabels(_rangeValues.start.toInt().toString(),
                     _rangeValues.end.toInt().toString());
              }
            });
          },
        ),
      );
    }

    return Scaffold(
        backgroundColor: getBackground(),
        appBar: AppBar(
          title: Text($L.of(context)!.settings),
          actions: <Widget>[
            new IconButton(
              tooltip: $L.of(context)?.apply,
              icon: new Icon(Icons.check),
              onPressed: () {
                globalStorage.lower = _rangeValues!.start.toInt();
                globalStorage.upper = _rangeValues!.end.toInt();
                globalStorage.save();
                BlocProvider.of<ThinkBloc>(context).add(ThinkEvent.reset);
                Navigator.of(context).pop(null);
              },
            ),
          ],
        ),
        body: new Container(
            padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${$L.of(context)!.resultInterval}:",
                      style: TextStyle(
                          fontSize: 30.0, fontWeight: FontWeight.bold)),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Row(
                      children: <Widget>[
                        Text(_rangeValues!.start.toInt().toString(),
                            style: TextStyle(
                                fontSize: 30.0, fontWeight: FontWeight.bold)),
                        Expanded(child: buildSlider()),
                        Text(_rangeValues!.end.toInt().toString(),
                            style: TextStyle(
                                fontSize: 30.0, fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                ])));
  }
}
