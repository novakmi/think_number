import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:think_number/about.dart';
import 'package:think_number/bloc.dart';
import 'package:think_number/body.dart';
import 'package:think_number/common.dart';
import 'package:think_number/l10n/localization.dart';
import 'package:think_number/settings.dart';

class ThinkNumberHomePage extends StatelessWidget {
  final _myBody = ThinkNumberBody();

  @override
  Widget build(BuildContext context) {
    Choice.buildChoices(context);

    PopupMenuItem<SelectedItem> makePopupMenuItem(Choices choiceVal) {
      Choice choice =
          Choice.choices.firstWhere((choice) => choice.val == choiceVal);
      return PopupMenuItem<SelectedItem>(
        value: SelectedItem(choice: choice, context: context),
        child: Text(choice.title),
      );
    }

    return Scaffold(
      backgroundColor: getBackground(),
      appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Text(
            $L.of(context)!.title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
          actions: <Widget>[
            // Popup menu based on description present here
            //https://www.developerlibs.com/2018/06/flutter-event-alert-with-pop-up.html
            PopupMenuButton<SelectedItem>(
              onSelected: _selectMenu,
              itemBuilder: (BuildContext context) {
                return [
                  makePopupMenuItem(Choices.settings),
                  makePopupMenuItem(Choices.about),
                  const PopupMenuDivider(height: 0),
                  makePopupMenuItem(Choices.exit),
                ];
              },
            )
          ]),
      body: _myBody,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          BlocProvider.of<ThinkBloc>(context).add(ThinkEvent.ok); // notify bloc button was pressed
        },
        tooltip: $L.of(context)!.confirm,
        child: Text(
          $L.of(context)!.ok,
          style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24),
        ), //Icon(Icons.add),
      ),
    );
  }

  void _selectMenu(SelectedItem selection) {
    if (selection.choice.val == Choices.exit) {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }
    if (selection.choice.val == Choices.settings) {
      Navigator.push(
        selection.context,
        MaterialPageRoute(builder: (context) => Settings()),
      );
    }
    if (selection.choice.val == Choices.about) {
      showDialog(
        context: selection.context,
        builder: (BuildContext context) => buildAboutDialog(selection.context),
      );
    }
  }
}

enum Choices { settings, about, exit }

class Choice {
  Choice({required this.title, required this.val});

  final String title;
  final Choices val;

  static List<Choice> choices = List<Choice>.empty();
  static String? choiceValue;

  static buildChoices(BuildContext context) {
    choices = [
      Choice(title: $L.of(context)!.settings + " ...", val: Choices.settings),
      Choice(title: $L.of(context)!.about + " ...", val: Choices.about),
      Choice(title: $L.of(context)!.exit, val: Choices.exit),
    ];
  }
}

class SelectedItem {
  SelectedItem({required this.choice, required this.context});

  final Choice choice;
  BuildContext context;
}
