// https://www.developerlibs.com/2018/06/flutter-event-alert-with-pop-up.html

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:think_number/common.dart';
import 'package:think_number/l10n/localization.dart';

//import 'package:flutter/gestures.dart';
//import 'package:url_launcher/url_launcher.dart';

// This about dialog is based on description present here
//https://www.developerlibs.com/2018/06/flutter-event-alert-with-pop-up.html
Widget buildAboutDialog(BuildContext context) {
  final String _aboutText = $L.of(context)!.about_init +
      '\n\n' +
      $L.of(context)!.version +
      ' ${getVersion()}\n\n';
  /*
  TapGestureRecognizer flutterTapRecognizer;

  void _openUrl(String url) async {
    // Close the about dialog.
    flutterTapRecognizer.dispose();
    Navigator.pop(context);

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  const String flutterUrl = 'https://flutter.io/';
  */

  Widget _buildAboutText(BuildContext context) {
    /*
    const TextStyle linkStyle = const TextStyle(
      color: Colors.blue,
      decoration: TextDecoration.underline,
    );

    flutterTapRecognizer = new TapGestureRecognizer()
      ..onTap = () => _openUrl(flutterUrl);
    */
    return RichText(
      text: TextSpan(
        text: _aboutText,
        style: const TextStyle(color: Colors.black87),
        children: <TextSpan>[
          TextSpan(text: $L.of(context)!.about_developed_with_flutter + ' '),
          TextSpan(
            text: 'Flutter',
            /*
            // https://github.com/flutter/flutter/issues/27852
            recognizer: flutterTapRecognizer,
            style: linkStyle,
            */
          ),
          const TextSpan(text: '.\n\n'),
          const TextSpan(text: 'GitLab repository:\n'),
          TextSpan(
            text: 'https://gitlab.com/novakmi/think_number',
            /*
            // https://github.com/flutter/flutter/issues/27852
            recognizer: flutterTapRecognizer,
            style: linkStyle,
            */
          ),
          const TextSpan(text: '.'),
        ],
      ),
    );
  }

  return AlertDialog(
    title: Text($L.of(context)!.about),
    content: SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildAboutText(context),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: FlutterLogo(
              size: 50,
            ),
          ),
          //_buildLogoAttribution(), // TODO not implemented (yet?)
        ],
      ),
    ),
    actions: <Widget>[
      new TextButton(
        style: TextButton.styleFrom(
          side: BorderSide(
            color: Theme.of(context).primaryColor,
          ),
        ),
        onPressed: () {
          /*
          flutterTapRecognizer.dispose();
          */
          Navigator.of(context).pop();
        },
        child: Text($L.of(context)!.dismiss),
      ),
    ],
  );
}
