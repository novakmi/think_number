// for now we implement storage as singleton
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  ///
  /// Singleton factory
  ///

  static final defaultLowerValue = 2;
  static final defaultUpperValue = 16;

  int lowerValue = defaultLowerValue;
  int upperValue = defaultUpperValue;

  int get lower => lowerValue;

  int get upper => upperValue;

  set lower(int lower) => lowerValue = lower;

  set upper(int lower) => upperValue = lower;

  static final Storage _storage = Storage._internal();

  factory Storage() {
    return _storage;
  }

  Storage._internal();

  load() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    lowerValue = prefs.getInt('lower') ?? defaultLowerValue;
    upperValue = prefs.getInt('upper') ?? defaultUpperValue;
  }

  save() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("lower", lowerValue);
    prefs.setInt("upper", upperValue);
  }
}

Storage globalStorage = Storage();
