import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:think_number/l10n/localization.dart';
import 'package:think_number/storage.dart';

class ThinkBloc extends Bloc<ThinkEvent, ThinkState> {
  static const _texts = [
    "think",
    "addtogether",
    "add",
    "divide",
    "subtract",
    "result"
  ];

  int _counter = 0;
  int _result = makeResult();

  ThinkBloc() : super(ThinkState(text: _texts[0], result: makeResult())) {
    on<ThinkEvent>((event, emit) {
      _counter++;
      if (_counter == _texts.length || event == ThinkEvent.reset) {
        _counter = 0;
        _result = makeResult();
      }
      emit(ThinkState(text: _texts[_counter], result: _result));
    });
  }

  // @override
  // Stream<ThinkState> mapEventToState(ThinkEvent event) async* {
  //   _counter++;
  //   if (_counter == _texts.length || event == ThinkEvent.reset) {
  //     _counter = 0;
  //     _result = makeResult();
  //   }
  //   yield ThinkState(text: _texts[_counter], result: _result);
  // }

  static final _rnd = new Random();

  static int makeResult() {
    int range = globalStorage.upper - globalStorage.lower;
    //print("range: ${range}");
    return _rnd.nextInt(range + 1) + globalStorage.lower;
  }
}

enum ThinkEvent { ok, reset }

class ThinkState {
  String text;
  int result;

  ThinkState({required this.text, required this.result});

  static String getStateLocalizedText(BuildContext context, ThinkState state) {
    late String text;
    switch (state.text) {
      case "think":
        text = $L.of(context)!.think;
        break;
      case "addtogether":
        text = $L.of(context)!.addtogether;
        break;
      case "add":
        text = $L.of(context)!.add;
        break;
      case "divide":
        text = $L.of(context)!.divide;
        break;
      case "subtract":
        text = $L.of(context)!.subtract;
        break;
      case "result":
        text = $L.of(context)!.result;
        break;
    }
    if (text.contains("{}")) {
      text = text.replaceFirst("{}", "${state.result * 2}");
    }
    return text;
  }
}
