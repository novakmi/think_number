import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:think_number/bloc.dart';
import 'package:think_number/common.dart';
import 'package:think_number/home.dart';
import 'package:think_number/l10n/localization.dart';
import 'package:think_number/l10n/localization_delegate.dart';
import 'package:think_number/storage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  initPackageInfo();
  globalStorage.load();
  runApp(ThinkNumberApp());
}

class ThinkNumberApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // BlocProvider must be above MaterialApp so bloc is accessible in other routes
    //https://www.didierboelens.com/2018/12/reactive-programming---streams---bloc---practical-use-cases/
    return BlocProvider(
      //bloc: _thinkBloc,
      create: (BuildContext context) => ThinkBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        localizationsDelegates: [
          const $LDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('cs', ''),
        ],
        onGenerateTitle: (context) => $L.of(context)!.title,
        home: ThinkNumberHomePage(),
      ),
    );
  }
}
