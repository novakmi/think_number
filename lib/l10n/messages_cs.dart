// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a cs locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'cs';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about" : MessageLookupByLibrary.simpleMessage("O aplikaci"),
    "about_developed_with_flutter" : MessageLookupByLibrary.simpleMessage("Aplikace byla vyvinuta ve frameworku"),
    "about_init" : MessageLookupByLibrary.simpleMessage("Tato aplikace představuje jednoduchý matematický trik.\n\nAplikace slouží ke zkoumání a učení Flutter frameworku."),
    "add" : MessageLookupByLibrary.simpleMessage("Přičti {}"),
    "addtogether" : MessageLookupByLibrary.simpleMessage("Ještě jednou tolik"),
    "apply" : MessageLookupByLibrary.simpleMessage("Použít"),
    "confirm" : MessageLookupByLibrary.simpleMessage("Potvrdit"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Zrušit"),
    "divide" : MessageLookupByLibrary.simpleMessage("Vyděl to dvěma"),
    "exit" : MessageLookupByLibrary.simpleMessage("Ukončit"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "result" : MessageLookupByLibrary.simpleMessage("Vyšlo ti:"),
    "resultInterval" : MessageLookupByLibrary.simpleMessage("Interval výsledku"),
    "settings" : MessageLookupByLibrary.simpleMessage("Nastavení"),
    "subtract" : MessageLookupByLibrary.simpleMessage("Odečti to číslo co jsi si myslel"),
    "think" : MessageLookupByLibrary.simpleMessage("Mysli si číslo"),
    "title" : MessageLookupByLibrary.simpleMessage("Mysli si Číslo"),
    "version" : MessageLookupByLibrary.simpleMessage("verze")
  };
}
