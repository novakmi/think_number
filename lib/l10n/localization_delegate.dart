import 'dart:async';

import 'package:flutter/material.dart';

import 'localization.dart';

class $LDelegate extends LocalizationsDelegate<$L> {
  const $LDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'cs'].contains(locale.languageCode);

  @override
  Future<$L> load(Locale locale) => $L.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<$L> old) => false;
}
