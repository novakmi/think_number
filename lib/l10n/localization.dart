//https://phraseapp.com/blog/posts/how-to-internationalize-a-flutter-app/
// https://flutter.dev/docs/development/accessibility-and-localization/internationalization

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:think_number/l10n/messages_all.dart';

class $L {
  static Future<$L> load(Locale locale) {
    final String name = locale.countryCode == null || locale.countryCode!.isEmpty
        ? locale.languageCode
        : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return $L();
    });
  }

  static $L? of(BuildContext context) => Localizations.of<$L>(context, $L);

  String get title => Intl.message(
        'Think a Number',
        name: 'title',
        desc: 'App title',
      );

  String get ok => Intl.message(
        'OK',
        name: 'ok',
        desc: 'Floating button text',
      );

  String get confirm => Intl.message(
        'Confirm',
        name: 'confirm',
        desc: 'Floating button tooltip',
      );

  String get think => Intl.message(
        'Think a secret number',
        name: 'think',
        desc: 'Think a secret number',
      );

  String get addtogether => Intl.message(
        'Add it together',
        name: 'addtogether',
        desc: 'Add it together',
      );

  String get add => Intl.message(
        'Add {}',
        name: 'add',
        desc: 'Add {}',
      );

  String get divide => Intl.message(
        'Divide by two',
        name: 'divide',
        desc: 'Divide by two',
      );

  String get subtract => Intl.message(
        'Subtract your secret number',
        name: 'subtract',
        desc: 'Subtract your secret number',
      );

  String get result => Intl.message(
        'The result is:',
        name: 'result',
        desc: 'The result is:',
      );

  String get about => Intl.message(
        'About',
        name: 'about',
        desc: 'About choice of menu, title of about dialog',
      );

  String get settings => Intl.message(
        'Settings',
        name: 'settings',
        desc: 'Settings choice of menu, title of about dialog',
      );

  String get exit => Intl.message(
        'Exit',
        name: 'exit',
        desc: 'Exit choice of menu',
      );

  String get about_init => Intl.message(
        'This application represents simple mathematical trick.\n\n'
            'Application itself is used as playground to test and learn Flutter framework.',
        name: 'about_init',
        desc: 'first part of about dialog',
      );

  String get version => Intl.message(
        'version',
        name: 'version',
        desc: 'version text in about dialog',
      );

  String get about_developed_with_flutter => Intl.message(
        'The application was developed with',
        name: 'about_developed_with_flutter',
        desc: 'part of about dialog telling developed with Flutter'
            '(Flutter link right after this text)',
      );

  String get dismiss => Intl.message(
        'Dismiss',
        name: 'dismiss',
        desc: 'Dismiss button in about dialog',
      );

  String get resultInterval => Intl.message(
        'Result interval',
        name: 'resultInterval',
        desc: 'Result interval text in settings',
      );

  String get apply => Intl.message(
        'Apply',
        name: 'apply',
        desc: 'Apply tooltip over check mark (in settings dialog).',
      );
}
