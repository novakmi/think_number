// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about" : MessageLookupByLibrary.simpleMessage("About"),
    "about_developed_with_flutter" : MessageLookupByLibrary.simpleMessage("The application was developed with"),
    "about_init" : MessageLookupByLibrary.simpleMessage("This application represents simple mathematical trick.\n\nApplication itself is used as playground to test and learn Flutter framework."),
    "add" : MessageLookupByLibrary.simpleMessage("Add {}"),
    "addtogether" : MessageLookupByLibrary.simpleMessage("Add it together"),
    "apply" : MessageLookupByLibrary.simpleMessage("Apply"),
    "confirm" : MessageLookupByLibrary.simpleMessage("Confirm"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Dismiss"),
    "divide" : MessageLookupByLibrary.simpleMessage("Divide by two"),
    "exit" : MessageLookupByLibrary.simpleMessage("Exit"),
    "ok" : MessageLookupByLibrary.simpleMessage("OK"),
    "result" : MessageLookupByLibrary.simpleMessage("The result is:"),
    "resultInterval" : MessageLookupByLibrary.simpleMessage("Result interval"),
    "settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "subtract" : MessageLookupByLibrary.simpleMessage("Subtract your secret number"),
    "think" : MessageLookupByLibrary.simpleMessage("Think a secret number"),
    "title" : MessageLookupByLibrary.simpleMessage("Think a Number"),
    "version" : MessageLookupByLibrary.simpleMessage("version")
  };
}
