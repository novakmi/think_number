# think_number

A Flutter project.

## License

MIT License

## About

This project represents simple Mathematical trick and is used as playground to investigate Flutter.

## References

- [Android version on Google Play](https://play.google.com/store/apps/details?id=com.github.novakmi.thinknumber)
- [GitLab repository](https://gitlab.com/novakmi/think_number)